import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import axios from 'axios'
import '@fortawesome/fontawesome-free/css/all.min.css'


/* Buefy + Bulma Framework */
import Buefy from 'buefy'
Vue.use(Buefy, {
  // defaultIconPack: ['fa','far','fas','fad']
  defaultIconPack: 'fa'
})

/* Vue2 Filters - Pipes, Helpers, etc */
import Vue2Filters from 'vue2-filters'
Vue.use(Vue2Filters)

/* Vue Moment Filter */
import VueMoment from 'vue-moment'
Vue.use(VueMoment)

/* Vuesax Framework */
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css' //Vuesax styles
import 'material-icons/iconfont/material-icons.css'
Vue.use(Vuesax, { })

/* Vue Content Placeholders */
import VueContentPlaceholders from 'vue-content-placeholders'
Vue.use(VueContentPlaceholders)

/* Lodash */
import VueLodash from 'vue-lodash'
import lodash from 'lodash'
 
/* Vue WYSIWYG */
import wysiwyg from 'vue-wysiwyg'
import 'vue-wysiwyg/dist/vueWysiwyg.css'
Vue.use(wysiwyg, {
  hideModules: { 
    "image": true,
    "link": true,
    "code": true
  }
})

// name is optional
Vue.use(VueLodash, { name: 'custom' , lodash: lodash })

Vue.config.productionTip = false
Vue.prototype.$http = axios.create({
  baseURL: process.env.VUE_APP_API_URL
})

window.vueApp = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
