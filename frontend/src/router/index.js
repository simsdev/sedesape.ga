import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'ComingSoon',
    component: () => import('../views/ComingSoon.vue'),
    meta: {
      title: "EM BREVE | SE DESAPEGA!"
    }
  },
  {
    path: '/produtos',
    name: 'Produtos',
    component: () => import('../views/Produtos.vue'),
    meta: {
      title: "Produtos | SE DESAPEGA!"
    },
    children: [
      {
        path: ':id',
        name: 'ProdutosDetail',
        component: () => import('../views/ProdutosDetail.vue'),
        props: true,
        meta: {
          title: "Produtos | SE DESAPEGA!"
        },
      },
    ]
  },
  {
    path: '/admin/gerenciar',
    name: 'Gerenciar',
    component: () => import('../views/Gerenciar.vue'),
    meta: {
      title: "Gerenciar Anúncios | SE DESAPEGA!"
    },
    children: [
      {
        path: ':id',
        name: 'GerenciarDetail',
        component: () => import('../views/GerenciarDetail.vue'),
        props: true,
        meta: {
          title: "Gerenciar Anúncios | SE DESAPEGA!"
        },
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// router.beforeResolve((to, from, next) => { });

router.beforeEach((to, from, next) => {

  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  // const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

  // Skip rendering meta tags if there are none.
  if(nearestWithMeta) {
    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
      const tag = document.createElement('meta');

      Object.keys(tagDef).forEach(key => {
        tag.setAttribute(key, tagDef[key]);
      });

      // We use this to track which meta tags we create, so we don't interfere with other ones.
      tag.setAttribute('data-vue-router-controlled', '');

      return tag;
    })
    // Add the meta tags to the document head.
    .forEach(tag => document.head.appendChild(tag));
  }

  next()
});

// router.afterEach((to, from) => { })

export default router
