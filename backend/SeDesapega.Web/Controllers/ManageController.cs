﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using SeDesapega.Core.Application.Products.Query;
using SeDesapega.Core.Domain.Entities;
using SeDesapega.Core.Domain.Models;
using SeDesapega.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Entities;
using Imagekit;

namespace SeDesapega.Web.Controllers
{
    //[Authorize]
    public class ManageController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<ManageController> _logger;

        public ManageController(ILogger<ManageController> logger, IMediator mediator) : base(logger)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<AuthParamResponse> ImagekitAuth()
        {
            return await _mediator.Send(new ImagekitAuthenticatorCommand());
        }

        [HttpPost]
        public async Task<JsonResponse<Product>> Add([FromBody] AddNewProductCommand body)
        {
            Product result = await _mediator.Send(body);
            return new JsonResponse<Product>(result, true);
        }

        [HttpPost]
        public async Task<JsonResponse<Product>> Save([FromBody] Product body)
        {
            Product result = await _mediator.Send(new SaveProductCommand(body));
            return new JsonResponse<Product>(result, true);
        }
    }
}
