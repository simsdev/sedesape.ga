﻿using MediatR;
using SeDesapega.Core.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace SeDesapega.Web.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    [Consumes("application/json")]
    [Produces("application/json")]
    public abstract class BaseController : ControllerBase
    {
        private IMediator _mediator;
        private ILogger<BaseController> _logger;
        protected IMediator Mediator => _mediator ?? (_mediator = (HttpContext.RequestServices.GetService(typeof(IMediator)) as IMediator));

        public BaseController(ILogger<BaseController> logger)
        {
            _logger = logger;
        }

        /* Maybe make it using IoC -. on Application */
        public string UserId
        {
            get
            {
                string userId = string.Empty;
                try
                {
                    string[] claimTypes = new string[] {
                        JwtRegisteredClaimNames.Sub,
                        "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"
                    };

                    if (HttpContext.User.HasClaim(c => claimTypes.Contains(c.Type)))
                        userId = HttpContext.User.Claims.FirstOrDefault(c => claimTypes.Contains(c.Type)).Value;
                }
                catch (Exception ex) { /*TODO: log*/ }

                return userId;
            }

        }
        //private User _user;
        //public User LoggedUser
        //{
        //    get
        //    {
        //        try
        //        {
        //            if (_user != null)
        //                return _user;

        //            /* make it using DI */
        //            _user = DB.Find<User>().Execute().Where(w => w.Auth0Token == this.UserId).FirstOrDefault();
        //        }
        //        catch (Exception ex) { /*TODO: log*/ }

        //        return _user;
        //    }
        //}
    }
}
