﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using SeDesapega.Core.Application.Products.Query;
using SeDesapega.Core.Domain.Entities;
using SeDesapega.Core.Domain.Models;
using SeDesapega.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Entities;

namespace SeDesapega.Web.Controllers
{
    //[Authorize]
    public class ProductController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<ProductController> _logger;

        public ProductController(ILogger<ProductController> logger, IMediator mediator) : base(logger)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<JsonResponse<List<Product>>> List()
        {
            List<Product> result = await _mediator.Send(new GetProductListQuery());
            return new JsonResponse<List<Product>>(result, true);
        }

        [HttpGet("{id}")]
        public async Task<JsonResponse<Product>> Get(string id)
        {
            Product result = await _mediator.Send(new GetProductBySlugQuery(id));
            return new JsonResponse<Product>(result, true);
        }

        [HttpPost]
        public async Task<JsonResponse<Product>> Add([FromBody] AddNewProductCommand body)
        {
            Product result = await _mediator.Send(body);
            return new JsonResponse<Product>(result, true);
        }
    }
}
