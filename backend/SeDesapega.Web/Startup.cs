using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using SeDesapega.Core.Domain.Models;
using Microsoft.Extensions.Options;
using SeDesapega.Core.Domain.Entities;
using System.Text.Json.Serialization;
using MongoDB.Entities;
using Newtonsoft.Json;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Interfaces;
using Newtonsoft.Json.Converters;
using MediatR;
using System.Reflection;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using SeDesapega.Core.Application.Products.Query;

namespace SeDesapega.Web
{
    public class Startup
    {
        public string ApiName => "SeDesape.ga API";
        public string ApiVersion => "v1";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            AppConfiguration.Setup(Configuration);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = $"https://{Configuration["Auth0:Domain"]}/";
                options.Audience = Configuration["Auth0:Audience"];
            });

            services.AddMongoDBEntities(Configuration["MongoDatabaseSettings:DatabaseName"],
                                        Configuration["MongoDatabaseSettings:Hostname"],
                                        Convert.ToInt32(Configuration["MongoDatabaseSettings:Port"]));

            services.AddMediatR(typeof(GetProductHandler).GetTypeInfo().Assembly);
            services.AddControllers()
                    .AddNewtonsoftJson(options =>
                    {
                        options.SerializerSettings.Converters.Add(new StringEnumConverter());
                        options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(this.ApiVersion, new OpenApiInfo { 
                    Title = this.ApiName, 
                    Version = this.ApiVersion,
                    //Description = string.Join("\n", GetType().Assembly.GetManifestResourceStream("SeDesapega.Web.Web.md").ReadAllLines()),
                    Extensions = this.OpenApiExtensions()
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                              Type = ReferenceType.SecurityScheme,
                              Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header
                        },
                        new List<string>()
                    }
                });
            });

            services.AddSwaggerGenNewtonsoftSupport();

            services.AddCors((o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
            })));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{this.ApiName} {this.ApiVersion}");
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCors("MyPolicy");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private Dictionary<string, IOpenApiExtension> OpenApiExtensions()
        {
            var extensionsDictionary = new Dictionary<string, IOpenApiExtension>();

            // ==== Add x-logo extension vendor 
            var xlogo = new OpenApiObject();
            //xlogo.Add("url", new OpenApiString("https://admin.portaloperadoras.com/assets/images/logo_vhchospitality.png"));
            xlogo.Add("url", new OpenApiString("/assets/images/logo_vhc_docs.png"));
            xlogo.Add("altText", new OpenApiString("SeDesape.ga"));
            extensionsDictionary.Add("x-logo", xlogo);

            return extensionsDictionary;
        }
    }
}
