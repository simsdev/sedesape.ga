﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeDesapega.Core.Domain
{
    public static class Helpers
    {
        public static bool Empty(this string obj)
        {
            return string.IsNullOrWhiteSpace(obj);
        }
    }
}
