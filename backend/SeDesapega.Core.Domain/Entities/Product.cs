﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using SeDesapega.Core.Domain.Enumerators;
using MongoDB.Entities;
using System.Linq;
using Newtonsoft.Json;
using SeDesapega.Core.Domain.Entities;
using SeDesapega.Core.Domain.Extensions;
using System.Runtime.CompilerServices;

namespace SeDesapega.Core.Domain.Entities
{
    public partial class Product : Entity
    {
        public Product(string productName, string productDescription, decimal productPrice, string whatsapp, string twitter, string facebook) : this()
        {
            this.Name = productName;
            this.Slug = productName.GenerateSlug();
            this.Description = productDescription;
            this.Price = productPrice;
            this.Photo = "https://picsum.photos/400/225"; //?blur=2";
            this.PhotoList = new List<Picture>();
            
            for(int i=0; i<5; i++)
                this.PhotoList.Add(new Picture("https://picsum.photos/1920x1080", "Mussum Ipsum, cacilds vidis litro abertis.", i));

            this.Whatsapp = whatsapp;
            this.Twitter = twitter;
            this.Facebook = facebook;
            this.DateCreated = DateTime.Now;
            this.DateLastChanged = DateTime.Now;
        }

        public Product()
        {
            this.InitOneToMany(() => Reports);
        }

        public string Name { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Photo { get; set; }
        public List<Picture> PhotoList { get; set; }
        public string Whatsapp { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateLastChanged { get; set; }

        [JsonIgnore]
        public Many<Report> Reports { get; set; }

        [Ignore]
        [JsonIgnore]
        public List<Report> ReportsParsed => this.Reports.ChildrenQueryable().ToList();

        [JsonIgnore]
        public One<ProductCategory> Category { get; set; }

        [Ignore]
        [JsonProperty("category")]
        public ProductCategory CategoryParsed => (this.Category != null ? this.Category.ToEntity() : null);
    }

    public class ProductCategory : Entity
    {
        public ProductCategory() { }
        public ProductCategory(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }
    }

    public class Picture
    {
        public string ImageId { get; set; }
        public string Image { get; set; }
        public string Caption { get; set; }
        public int Order { get; set; }

        public Picture() { }
        public Picture(string image, string caption, int order = 0) : this(null, image, caption, order) { }
        public Picture(string imageId, string image, string caption, int order)
        {
            this.ImageId = imageId;
            this.Image = image;
            this.Caption = caption;
            this.Order = order;
        }
    }
}
