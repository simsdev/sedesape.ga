﻿using MongoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeDesapega.Core.Domain.Entities
{
    public class Report : Entity
    {
        public Report() { }

        public Report(string title, string message, string reporterName, string reporterEmail, string ip, DateTime date, ReportCategory category)
        {
            this.Title = title;
            this.Message = message;
            this.ReporterName = reporterName;
            this.ReporterEmail = reporterEmail;
            this.Ip = ip;
            this.Date = date;
            this.Category = category;
        }

        public string Title { get; set; }
        public string Message { get; set; }
        public string ReporterName { get; set; }
        public string ReporterEmail { get; set; }
        public string Ip { get; set; }
        public string Icon { get; set; }
        public DateTime Date { get; set; }
        public One<ReportCategory> Category { get; set; }
    }

    public class ReportCategory : Entity
    {
        public ReportCategory() { }
        public ReportCategory(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }
    }
}
