﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeDesapega.Core.Domain.Models
{
    public static class AppConfiguration
    {
        public static string ImageKitPublicKey { get; set; }
        public static string ImageKitPrivateKey { get; set; }
        public static string ImageKitUrlEndpoint { get; set; }

        public static void Setup(IConfiguration config)
        {
            AppConfiguration.ImageKitPublicKey = config["ImageKit:PublicKey"];
            AppConfiguration.ImageKitPrivateKey = config["ImageKit:PrivateKey"];
            AppConfiguration.ImageKitUrlEndpoint = config["ImageKit:UrlEndpoint"];
        }
    }
}
