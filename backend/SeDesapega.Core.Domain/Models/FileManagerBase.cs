﻿using ByteSizeLib;
using SeDesapega.Core.Domain.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeDesapega.Core.Domain.Models
{
    public class FileManager
    {
        [JsonIgnore]
        public const char PATH_SEPARATOR = '/';
    }

    public class FileManagerBase : FileManager
    {
        public string ServerID { get; set; }
        public string BasePath { get; set; }
        public string ActualPath
        {
            get
            {
                if (this.BasePathList == null || this.BasePathList.Length == 0)
                    return string.Empty;

                return this.BasePathList[this.BasePathList.Length - 1];
            }
        }
        public string[] BasePathList => this.BasePath.RemoveFirst("/").RemoveLast("/").Split(PATH_SEPARATOR) ?? new string[1];
        public List<FileManagerFile> Files { get; set; }
    }

    public class FileManagerFile : FileManager
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Extension { get; set; }
        public string FilePath { get; set; }
        public string[] FilePathStringList => this.FullFilePath.RemoveFirst("/").RemoveLast("/").Split(PATH_SEPARATOR) ?? new string[1];
        public string PathWithoutBasePath { get; set; }
        public string ActualPath
        {
            get
            {
                if (this.FilePathStringList == null || this.FilePathStringList.Length == 0)
                    return string.Empty;

                return this.FilePathStringList[this.FilePathStringList.Length - 1];
            }
        }

        public string FilePathWithoutBasePath { get; set; }
        public string FullFilePath { get; set; }
        public long SizeInBytes { get; set; }
        public string SizeString
        {
            get
            {
                var size = ByteSize.FromBytes(this.SizeInBytes);
                if (size.MegaBytes > 1024)
                    return size.GigaBytes.ToString("N2") + " GB";
                else if (size.KiloBytes > 1024)
                    return size.MegaBytes.ToString("N2") + " MB";
                else
                    return size.KiloBytes.ToString("N2") + " KB";
            }
        }
        public FileType FileType { get; set; }
        public string FileTypeString => this.FileType.ToString();
        public DateTime Modified { get; set; }
        public bool ShouldHideFileOrDirectory { get; set; }
    }

    public enum FileType
    {
        None,
        Directory,
        File,
        BackDirectory,
        Link
    }
}
