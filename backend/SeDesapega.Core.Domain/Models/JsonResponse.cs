﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeDesapega.Core.Domain.Models
{
    public class JsonResponse<T>
    {
        public JsonResponse(T obj, bool success)
        {
            this.Data = obj;
            this.Result = success;
        }

        public T Data { get; set; }

        public bool Result { get; set; }
    }
}
