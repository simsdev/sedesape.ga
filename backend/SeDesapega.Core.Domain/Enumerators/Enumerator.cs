﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeDesapega.Core.Domain.Enumerators
{
    public enum ENotificationType
    {
        Primary,
        Success,
        Danger,
        Warning
    }
}
