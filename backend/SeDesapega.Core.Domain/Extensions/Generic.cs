﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace SeDesapega.Core.Domain.Extensions
{
    public static class ObjectToDictionaryHelper
    {
        //public static IEnumerable<SelectListItem> ToSelectList<T>(this IEnumerable<T> items, Func<T, string> text, Func<T, string> value = null, Func<T, Boolean> selected = null)
        //{
        //    return items.Select(p => new SelectListItem
        //    {
        //        Text = text.Invoke(p),
        //        Value = (value == null ? text.Invoke(p) : value.Invoke(p)),
        //        Selected = selected == null ? false : selected.Invoke(p)
        //    });
        //}

        public static IDictionary<string, object> ToDictionary(this object source)
        {
            return source.ToDictionary<object>();
        }

        public static IDictionary<string, T> ToDictionary<T>(this object source)
        {
            if (source == null)
                ThrowExceptionWhenSourceArgumentIsNull();

            var dictionary = new Dictionary<string, T>();
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(source))
                AddPropertyToDictionary<T>(property, source, dictionary);
            return dictionary;
        }

        private static void AddPropertyToDictionary<T>(PropertyDescriptor property, object source, Dictionary<string, T> dictionary)
        {
            object value = property.GetValue(source);
            if (IsOfType<T>(value))
                dictionary.Add(property.Name, (T)value);
        }

        private static bool IsOfType<T>(object value)
        {
            return value is T;
        }

        private static void ThrowExceptionWhenSourceArgumentIsNull()
        {
            throw new ArgumentNullException("source", "Unable to convert object to a dictionary. The source object is null.");
        }
    }

    public static class GenericMethods
    {
        public static byte[] ReadToEnd(this System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }

        public static string NormalizeText(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark).ToArray();
            return new string(chars).Normalize(NormalizationForm.FormC);
        }

        public static string GenerateSlug(this string phrase)
        {
            string str = phrase.RemoveAccent().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        public static string RemoveAccent(this string text)
        {
            StringBuilder sbReturn = new StringBuilder();
            var arrayText = text.Normalize(NormalizationForm.FormD).ToCharArray();
            foreach (char letter in arrayText)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                    sbReturn.Append(letter);
            }
            return sbReturn.ToString();
        }

        public static int Int32ConvertFromDBVal(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return 0;
            }
            else
            {
                return (int)obj;
            }
        }

        public static Int16 Int16ConvertFromDBVal(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return 0;
            }
            else
            {
                return (Int16)obj;
            }
        }

        public static decimal DecimalConvertFromDBVal(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return 0;
            }
            else
            {
                return (decimal)obj;
            }
        }

        public static long LongConvertFromDBVal(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return 0;
            }
            else
            {
                return (long)obj;
            }
        }

        public static String StringConvertFromDBVal(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return String.Empty;
            }
            else
            {
                return (String)obj;
            }
        }

        public static bool BooleanConvertFromDBVal(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return false;
            }
            else
            {
                return (bool)obj;
            }
        }

        public static bool BooleanBitConvertFromDBVal(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return false;
            }
            else
            {
                try { return ((Int64)obj > 0 ? true : false); } catch { }
                return ((UInt64)obj > 0 ? true : false);
            }
        }

        public static DateTime? DateTimeConvertFromDBVal(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return null;
            }
            else
            {
                return (DateTime)obj;
            }
        }

        /// <summary>
        /// Retorna string com tamanho específico
        /// </summary>
        public static string Truncate(this string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
            }
            return source;
        }

        public static bool ValidaData(string data)
        {
            bool ret = true;
            if (data.Length >= 8)
            {
                System.DateTime date = default(System.DateTime);
                date = System.Convert.ToDateTime(data);
                int dia = date.Day;
                int mes = date.Month;
                return dia < 32 && mes < 13 && ret;
            }
            return false;
        }

        public static void ForEach(this HybridDictionary dict, Action<DictionaryEntry> action)
        {
            foreach (DictionaryEntry pair in dict)
            {
                action(pair);
            }
        }

        public static DictionaryEntry Find(this HybridDictionary dict, Func<DictionaryEntry, Boolean> action)
        {
            var retorno = new HybridDictionary();
            foreach (DictionaryEntry pair in dict)
            {
                if (action(pair))
                {
                    retorno.Add(pair.Key, pair.Value);
                    return pair;
                }


            }
            return new DictionaryEntry("", "");

        }


        public static HybridDictionary ConvertInstanceToHashtable<T>(this T parametro)
            where T : class, new()
        {

            var retorno = new HybridDictionary();

            //CRIO EMPTY INSTANCE PARA FAZER O CHECK 
            //DOS VALORES DEFAULT COM OS VALORES PREENCHIDOS DO OBJETO
            var emptyInstance = new T();

            parametro = parametro.InstantiateIfNull();

            //PEGO TODAS AS PROPRIEDADES DA CLASSE DO OBJETO PASSADO COMO PARAMETRO.
            var properties = parametro.GetType().GetProperties().ToList();
            var fields = parametro.GetType().GetFields().ToList();

            //FOREACH PROPRIEDADE VALIDA, ADICIONO FILTRO NO HASH.
            properties.ForEach(prop =>
            {
                var propertyName = prop.Name;
                var propertyValueWarm = prop.GetValue(parametro, new object[] { });
                var propertyValueCold = prop.GetValue(emptyInstance, new object[] { });

                //TODO: CHECAR OS CASTS DOS VALORES DAS PROPERTIES PARA STRINGS A SEREM INFORMADAS PARA O SOLR. TokenizeDTOToHashtable
                if (propertyValueCold != propertyValueWarm)
                    retorno.Add(propertyName, Convert.ToString(propertyValueWarm));
            });

            fields.ForEach(prop =>
            {
                var propertyName = prop.Name;
                var propertyValueWarm = prop.GetValue(parametro);
                var propertyValueCold = prop.GetValue(emptyInstance);

                //TODO: CHECAR OS CASTS DOS VALORES DAS PROPERTIES PARA STRINGS A SEREM INFORMADAS PARA O SOLR. TokenizeDTOToHashtable
                if (propertyValueCold != propertyValueWarm)
                    retorno.Add(propertyName, Convert.ToString(propertyValueWarm));
            });

            return retorno;
        }

        /// <summary>
        /// REPRODUÇÃO DA FUNÇÃO SET TIMEOUT DO JAVASCRIPT
        /// </summary>
        /// <param name="timeoutInMilliSeconds">TEMPO PARA A EXECUÇÃO DO CODIGO</param>
        /// <param name="contextAction">CODIGO A SER EXECUTADO</param>
        public static void SetTimeout(this Int32 timeoutInMilliSeconds, Action contextAction)
        {
            new Thread(() =>
            {
                Thread.Sleep(timeoutInMilliSeconds);
                contextAction();
            })
                .Start();
        }

        public static Boolean IsIn<T>(this T item, params T[] itens)
        {
            if (itens == null || itens.Length == 0) { return false; }

            return IsIn(item, itens.AsEnumerable());
        }

        public static Boolean IsIn<T>(this T item, IEnumerable<T> itens)
        {
            return itens.ToList().Contains(item);
        }

        /// <summary>
        /// Efetua chamada de método ToString para PT-BR
        /// </summary>
        /// <typeparam name="T">IConvertible</typeparam>
        /// <param name="item">instancia de IConvertible</param>
        /// <returns>string formatando o valor com CultureInfo pt-BR</returns>
        public static String ToPtBrString<T>(this T item) where T : IConvertible
        {
            return item.ToString(new CultureInfo("pt-BR"));
        }

        /// <summary>
        /// Efetua chamada de método ToString para EN-US
        /// </summary>
        /// <typeparam name="T">IConvertible</typeparam>
        /// <param name="item">instancia de IConvertible</param>
        /// <returns>string formatando o valor com CultureInfo en-US</returns>
        public static String ToEnUsString<T>(this T item) where T : IConvertible
        {
            return item.ToString(new CultureInfo("en-US"));
        }

        public static Decimal ToDecimalOrZero<T>(this T item) where T : IConvertible
        {
            try
            {
                return Convert.ToDecimal(item);
            }
            catch
            {
                return 0;
            }
        }

        public static Decimal ToDecimal<T>(this T item) where T : IConvertible
        {
            return Convert.ToDecimal(item);
        }

        public static Double ToDouble<T>(this T item) where T : IConvertible
        {
            return Convert.ToDouble(item);
        }

        public static Double ToDoubleOrZero<T>(this T item) where T : IConvertible
        {
            try
            {
                return Convert.ToDouble(item);
            }
            catch
            {
                return 0;
            }
        }

        //Retorna um DATETIME. Recebe uma data em STRING no formato pt-br
        public static DateTime ToDateTimePtBr<T>(this T item) where T : IConvertible
        {
            try
            {
                return item.ToDateTime(new CultureInfo("pt-BR"));
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        public static Int32 ToIntOrZero<T>(this T item) where T : IConvertible
        {
            if (item == null)
            {
                return 0;
            }

            try
            {
                return Convert.ToInt32(item);
            }
            catch (Exception)
            {
            }

            return 0;
        }

        public static T InstantiateIfNull<T>(this T self) where T : class, new()
        {
            var retorno = new T();
            if (self != null)
            {
                retorno = self;
            }

            return retorno;
        }

        public static string EmptyIfNull<String>(this String self) where String : class, new()
        {
            var retorno = "";
            if (self != null)
            {
                retorno = self.ToString();
            }

            return retorno;
        }

        public static Boolean IsNumeric(this String toCheck)
        {
            var retorno = false;
            var chararray = toCheck.ToCharArray().ToList();
            retorno = chararray.TrueForAll(Char.IsNumber);
            return retorno;
        }

        public static Decimal SumCheck<T>(this List<T> source, Func<T, Decimal> selector)
            where T : new()
        {
            source = source.InstantiateIfNull();

            return source.Count > 0
                       ? source.ToList().Sum(selector)
                       : 0M;
        }

        public static Double SumCheck<T>(this List<T> source, Func<T, Double> selector)
            where T : new()
        {
            source = source.InstantiateIfNull();

            return source.Count > 0
                       ? source.ToList().Sum(selector)
                       : 0D;
        }

        /// <summary>
        /// Retorna o primeiro elemento da lista ou uma instância vazia.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T FirstOrDirty<T>(this List<T> source) where T : new()
        {
            return source.InstantiateIfNull().Count > 0 ? source.First() : new T();
        }


        /// <summary>
        /// Retorna o último elemento da lista ou uma instância vazia.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T LastOrDirty<T>(this List<T> source) where T : new()
        {
            return source.InstantiateIfNull().Count > 0 ? source.Last() : new T();
        }
        public static Boolean AllPropertiesAreEqual<T, U>(this List<T> source, Func<T, U> obterValorInicial, Func<T, U, Boolean> predicate)
        {

            Boolean retorno = true;

            //SE A LISTA NÃO POSSUIR NENHUM ELEMENTO, RETORNO TRUE
            if (source.Count == 0) return true;


            if (retorno)
            {
                //OBTENHO O VALOR INICIAL.
                var valorInicial = obterValorInicial(source.First());


                //PARA CADA ELEMENTO NA LISTA, VOU ITERANDO E DESCOBRINDO SE O VALOR DO ITEM CORRENTE É IGUAL AO VALOR INICIAL.
                source.ForEach(item =>
                {
                    if (retorno)
                    {
                        if (!predicate(item, valorInicial))
                        {
                            retorno = false;
                        }
                    }
                });
            }

            return retorno;
        }



        public static String Escape(this String s)
        {
            var retorno = s;// Microsoft.JScript.GlobalObject.escape(s);
            return retorno;
        }

        public static String Unescape(this String s)
        {
            var retorno = s;//Microsoft.JScript.GlobalObject.unescape(s);
            return retorno;
        }

        public static String ToPtBrFormatMoney<T>(this T item) where T : IConvertible
        {
            return item.ToPtBrFormatMoney(false, false);
        }

        public static String ToPtBrFormatMoney<T>(this T item, Boolean sinalNegativo) where T : IConvertible
        {
            return item.ToPtBrFormatMoney(sinalNegativo, false);
        }

        public static String ToPtBrFormatMoney<T>(this T item, Boolean sinalNegativo, Boolean exibirSifrao) where T : IConvertible
        {
            String retorno = String.Empty;
            Decimal valor = item.ToDecimalOrZero();

            retorno = String.Format("{0:###,###,###,##0.00}", valor);

            if (valor != 0 && sinalNegativo)
            {
                retorno = retorno.Insert(0, "- ");
            }

            if (exibirSifrao)
            {
                retorno = retorno.Insert(0, "R$ ");
            }

            return retorno;
        }

        public static Boolean ToBoolean<T>(this T item) where T : IConvertible
        {
            return Convert.ToBoolean(item);
        }


        public static T Cast<T>(this Object item)
        {
            return (T)item;
        }

        public static Boolean IsNullOrEmpty(this String valor)
        {
            return String.IsNullOrEmpty(valor);
        }


        public static void ForEachWithIndex<T>(this List<T> list, Action<T, Int32> action)
        {
            var index = 0;
            list.ForEach(t => action(t, index++));
        }

        public static List<T> Top<T>(this List<T> @self, Int32 quantidade)
        {
            @self = @self.InstantiateIfNull();

            if (quantidade > self.Count)
            {
                quantidade = self.Count;
            }

            return self.ToList().Take(quantidade).ToList();
        }

        public static void AddRangeOverride<TKey, TValue>(this Dictionary<TKey, TValue> dic, Dictionary<TKey, TValue> dicToAdd)
        {
            dicToAdd.ForEach(x => dic[x.Key] = x.Value);
        }

        public static void AddRangeNewOnly<TKey, TValue>(this Dictionary<TKey, TValue> dic, Dictionary<TKey, TValue> dicToAdd)
        {
            dicToAdd.ForEach(x => { if (!dic.ContainsKey(x.Key)) dic.Add(x.Key, x.Value); });
        }

        public static void AddRange<TKey, TValue>(this Dictionary<TKey, TValue> dic, Dictionary<TKey, TValue> dicToAdd)
        {
            dicToAdd.ForEach(x => dic.Add(x.Key, x.Value));
        }

        public static bool ContainsKeys<TKey, TValue>(this Dictionary<TKey, TValue> dic, IEnumerable<TKey> keys)
        {
            bool result = false;
            keys.ForEachOrBreak((x) => { result = dic.ContainsKey(x); return result; });
            return result;
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var item in source)
                action(item);
        }

        public static void ForEachOrBreak<T>(this IEnumerable<T> source, Func<T, bool> func)
        {
            foreach (var item in source)
            {
                bool result = func(item);
                if (result) break;
            }
        }


        public static IEnumerable<string> ReadAllLines(this Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                var lines = reader.ReadAllLines();
                return lines;
            }
        }

        public static List<string> ReadAllLines(this StreamReader reader)
        {
            List<string> lines = new List<string>();

            while (reader.Peek() >= 0) // reading the old data
                lines.Add(reader.ReadLine());

            return lines;
        }
    }

    public static class UnixDateTimeHelper
    {
        private const String InvalidUnixEpochErrorMessage = "Unix epoc starts January 1st, 1970";

        /// <summary>
        ///   Convert a long into a DateTime
        /// </summary>
        public static DateTime UnixTimeToDateTime(this Int64 self)
        {
            var retorno = DateTime.Now;
            var ret = new DateTime(1970, 1, 1);
            retorno = ret.AddMilliseconds(Convert.ToDouble(self));
            return retorno;
        }

        /// <summary>
        ///   Convert a DateTime into a long
        /// </summary>
        public static Int64 DateTimeToUnixTime(this DateTime self)
        {

            if (self == DateTime.MinValue)
            {
                return 0;
            }

            var epoc = new DateTime(1970, 1, 1);
            var delta = self - epoc;

            if (delta.TotalSeconds < 0) throw new ArgumentOutOfRangeException(InvalidUnixEpochErrorMessage);

            return (long)delta.TotalSeconds;
        }
    }

    public static class UtilHelper
    {
        /// <summary>
        ///   Set first character of the word to lower
        /// </summary>
        public static string FirstCharacterToLower(this string str)
        {
            if (String.IsNullOrEmpty(str) || Char.IsLower(str, 0))
                return str;

            return Char.ToLowerInvariant(str[0]).ToString() + str.Substring(1);
        }

        public static string GetParents(int noOfLevels, string currentpath, bool isLinux)
        {
            string path = "";
            for (int i = 0; i < noOfLevels; i++)
                path += @".." + (isLinux ? "/" : @"\");
            //path += currentpath;
            return path;
        }

        public static string GetParents(string mainPath, string currentPath, bool isLinux)
        {
            string path = "";
            int noOfLevels = currentPath.Replace(mainPath, "").Split((isLinux ? '/' : '\\')).Length;

            for (int i = 1; i < noOfLevels; i++)
                path += @".." + (isLinux ? "/" : @"\");
            //path += currentpath;
            return path;
        }

        public static string RemoveFirst(this string text, string character, bool removeIfStartsWith = true)
        {
            if (text.Length < 1) return text;

            if (removeIfStartsWith)
                if (text.StartsWith(character)) return text.Remove(0, 1);
                else return text;
            else return text.Remove(text.ToString().IndexOf(character), text.Length - 1);

        }

        public static string RemoveLast(this string text, string character, bool removeIfEndsWith = true)
        {
            if (text.Length < 1) return text;

            if (removeIfEndsWith)
                if (text.EndsWith(character)) return text.Remove(text.Length - 1, 1);
                else return text;
            else return text.Remove(text.ToString().LastIndexOf(character), character.Length);
        }
    }
}
