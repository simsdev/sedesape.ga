﻿using MediatR;
using SeDesapega.Core.Application._Handlers;
using SeDesapega.Core.Domain.Entities;
using MongoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace SeDesapega.Core.Application.Products.Query
{
    public class GetProductListHandler : IRequestHandler<GetProductListQuery, List<Product>>
    {
        public GetProductListHandler() { }

        public async Task<List<Product>> Handle(GetProductListQuery request, CancellationToken token)
        {
            List<Product> products = await DB.Find<Product>().ExecuteAsync(token);

            products.ForEach(f =>
            {
                f.PhotoList = f.PhotoList.OrderBy(o => o.Order).ToList();
            });

            return products;
        }
    }

    public class GetProductListQuery : IRequest<List<Product>>
    {

    }
}
