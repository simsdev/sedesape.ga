﻿using MediatR;
using SeDesapega.Core.Application._Handlers;
using SeDesapega.Core.Domain.Entities;
using MongoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace SeDesapega.Core.Application.Products.Query
{
    public class GetProductHandler : IRequestHandler<GetProductQuery, Product>,
                                     IRequestHandler<GetProductBySlugQuery, Product>
    {
        public GetProductHandler() { }

        public async Task<Product> Handle(GetProductQuery request, CancellationToken token = default)
        {
            Product product = await DB.Find<Product>().OneAsync(request.Id, token);
            return product;
        }

        public async Task<Product> Handle(GetProductBySlugQuery request, CancellationToken token = default)
        {
            Product product = DB.Queryable<Product>()
                                .Where(w => w.Slug == request.Id)
                                .SingleOrDefault();

            return product;
        }
    }

    public class GetProductQuery : IRequest<Product>
    {
        public GetProductQuery() { }

        public GetProductQuery(string id) : this()
        {
            this.Id = id;
        }

        public string Id { get; set; }
    }

    public class GetProductBySlugQuery : IRequest<Product>
    {
        public GetProductBySlugQuery() { }

        public GetProductBySlugQuery(string id) : this()
        {
            this.Id = id;
        }

        public string Id { get; set; }
    }
}
