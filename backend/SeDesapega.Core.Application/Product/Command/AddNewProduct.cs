﻿using MediatR;
using SeDesapega.Core.Application._Handlers;
using SeDesapega.Core.Domain.Entities;
using MongoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeDesapega.Core.Application.Products.Query
{
    public class AddNewProductHandler : IRequestHandler<AddNewProductCommand, Product>
    {
        public AddNewProductHandler() { }

        public async Task<Product> Handle(AddNewProductCommand request, CancellationToken token)
        {
            Product product = new Product(request.ProductName, request.ProductDescription, request.ProductPrice, request.Whatsapp, request.Twitter, request.Facebook);
            await product.SaveAsync(cancellation: token);

            return product;
        }
    }

    public class AddNewProductCommand : IRequest<Product>
    {
        public AddNewProductCommand() { }
        public AddNewProductCommand(string productName, string productDescription, decimal productPrice, string whatsapp, string twitter, string facebook)
        {
            this.ProductName = productName;
            this.ProductDescription = productDescription;
            this.ProductPrice = productPrice;
            this.Whatsapp = whatsapp;
            this.Twitter = twitter;
            this.Facebook = facebook;
        }

        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public decimal ProductPrice { get; set; }
        public string Whatsapp { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
    }
}
