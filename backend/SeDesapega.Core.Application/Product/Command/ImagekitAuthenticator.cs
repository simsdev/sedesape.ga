﻿using MediatR;
using SeDesapega.Core.Application._Handlers;
using SeDesapega.Core.Domain.Entities;
using MongoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Imagekit;
using SeDesapega.Core.Domain.Models;

namespace SeDesapega.Core.Application.Products.Query
{
    public class ImagekitAuthenticatorHandler : IRequestHandler<ImagekitAuthenticatorCommand, AuthParamResponse>
    {
        public ImagekitAuthenticatorHandler() { }

        public async Task<AuthParamResponse> Handle(ImagekitAuthenticatorCommand request, CancellationToken token)
        {
            return new ServerImagekit(AppConfiguration.ImageKitPublicKey, AppConfiguration.ImageKitPrivateKey, AppConfiguration.ImageKitUrlEndpoint).GetAuthenticationParameters();
        }
    }

    public class ImagekitAuthenticatorCommand : IRequest<AuthParamResponse>
    {
        public ImagekitAuthenticatorCommand() { }
    }
}