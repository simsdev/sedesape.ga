﻿using MediatR;
using SeDesapega.Core.Application._Handlers;
using SeDesapega.Core.Domain.Entities;
using MongoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;

namespace SeDesapega.Core.Application.Products.Query
{
    public class SaveProductHandler : IRequestHandler<SaveProductCommand, Product>
    {
        public SaveProductHandler() { }

        public async Task<Product> Handle(SaveProductCommand request, CancellationToken token)
        {
            Product dbProduct = await DB.Queryable<Product>().Where(w => w.ID == request.Product.ID).FirstOrDefaultAsync(token);

            if (dbProduct == null)
                throw new Exception("Product ID not found");

            // Update Values
            dbProduct.Name = request.Product.Name;
            dbProduct.Slug = request.Product.Slug;
            dbProduct.Price = request.Product.Price;
            dbProduct.Whatsapp = request.Product.Whatsapp;
            dbProduct.Facebook = request.Product.Facebook;
            dbProduct.Twitter = request.Product.Twitter;
            dbProduct.Photo = request.Product.Photo;
            dbProduct.PhotoList = request.Product.PhotoList;
            dbProduct.Description = request.Product.Description;
            dbProduct.DateLastChanged = request.Product.DateLastChanged;

            // Save in DB
            await dbProduct.SaveAsync(cancellation: token);

            return dbProduct;
        }
    }

    public class SaveProductCommand : IRequest<Product>
    {
        public SaveProductCommand() { }
        public SaveProductCommand(Product product)
        {
            this.Product = product;
        }

        public Product Product { get; set; }
    }
}
