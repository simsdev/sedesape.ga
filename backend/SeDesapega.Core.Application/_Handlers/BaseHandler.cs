﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeDesapega.Core.Application._Handlers
{
    public interface BaseHandler<T1, T2>
    {
        Task<T2> Handle(T1 request, CancellationToken token = default);
    }
}

